import logging

import requests


def get_coin_data(coin_id):
    response = requests.get(url="https://api.minerstat.com/v2/coins?list={}".format(coin_id))

    if response and response.status_code == 200:
        return response.json()
    else:
        logging.error('Error getting price for: ', coin_id)


def get_coin_price(coin_id):
    coin_data = get_coin_data(coin_id)[0]

    price = coin_data.get('price', None)
    timestamp = coin_data.get('updated', None)

    if timestamp and price:
        logging.info('Time: {} - Price for coin: {} is: {}'.format(timestamp, coin_id, price))
        return timestamp, price
    else:
        logging.error('No price available for: ', coin_id)
