import logging
import os
import time

from flask import Flask, request
from service import get_coin_price

logging.basicConfig(level=logging.INFO)

app = Flask(__name__)


@app.route('/')
def get_default_response():
    response = """
    <p>This application tracks crypto prices.</p>
    """

    return response


@app.route('/start')
def get_price():
    coin_id = request.args.get('coin_id', default='BTC', type=str)
    file_name = get_file_name(coin_id)

    if os.path.exists(file_name):
        logging.info('Cleaning up old log file.')
        os.remove(coin_id)

    while True:
        with open(file_name, 'a') as f:
            timestamp, price = get_coin_price(coin_id)
            f.write('Time: {}, Coin: {}, Price: {}'.format(timestamp, coin_id, price) + '\n')
            time.sleep(1)


@app.route('/read')
def read_price():
    coin_id = request.args.get('coin_id', default='BTC', type=str)
    file_name = get_file_name(coin_id)

    if not os.path.exists(file_name):
        return 'File does not exist. Please try again!'

    with open(file_name, 'r') as f:
        response = f.read()

    return response.replace('\n', '<br>')


def get_file_name(coin_id):
    return 'coin-{}'.format(coin_id)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5002)
